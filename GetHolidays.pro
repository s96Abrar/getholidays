QT       += core gui network widgets

TEMPLATE = app
TARGET = GetHolidays

VERSION = 1.0.0

CONFIG += thread silent build_all c++11

# Build location
BUILD_PREFIX = $$(CA_BUILD_DIR)
isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-plugins/
OBJECTS_DIR   = $$BUILD_PREFIX/obj-plugins/
RCC_DIR       = $$BUILD_PREFIX/qrc-plugins/
UI_DIR        = $$BUILD_PREFIX/uic-plugins/

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        DEFINES += VERSION_TEXT=\"\\\"$${VERSION}\\\"\"

        INSTALLS += target
        target.path = $$PREFIX/bin
}

SOURCES += \
    holidaymanage.cpp \
    main.cpp \
    generateholidays.cpp

HEADERS += \
    generateholidays.h \
    global.h \
    holidaymanage.h

FORMS += \
    generateholidays.ui
