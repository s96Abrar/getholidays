# Get Holidays

## A program which will get holidays for a country from [caledarific](https://calendarific.com).

This program needs a key for the API. It will give the holidays output by date and the name of the holiday with a comma separated (CSV).

Ex. ``01/01/2019,New Year's Day``

This program is created only for generating holidays which will be used in [CoreTime](https://gitlab.com/cubocore/coretime).
