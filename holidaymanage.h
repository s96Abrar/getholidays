/*
    *
    * This file is a part of GetHolidays.
    * Gets the holiday date and name for individual country and year.
    * Copyright 2019 Abrar
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#ifndef HOLIDAYMANAGE_H
#define HOLIDAYMANAGE_H

#include <QObject>
#include <QMap>
#include <QNetworkReply>
#include <QNetworkSession>
#include <QNetworkAccessManager>

#include "global.h"

class holidayManage : public QObject
{
    Q_OBJECT
public:
    explicit holidayManage(QString apiKey, QString countryCode, QString year, QObject *parent = nullptr);

signals:
    void holidaysCollected(CalendarData data);

public slots:

private:
    QString m_apiKey;
    QString m_countryCode;
    QString m_year;
    QNetworkAccessManager *nam;
    QNetworkSession *ns;

    void openNetworkSession();
    void handleHolidayData(QNetworkReply *reply);
};

#endif // HOLIDAYMANAGE_H
