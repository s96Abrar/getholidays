/*
    *
    * This file is a part of GetHolidays.
    * Gets the holiday date and name for individual country and year.
    * Copyright 2019 Abrar
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QFile>
#include <QDate>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>

#include "holidaymanage.h"
#include "generateholidays.h"
#include "ui_generateholidays.h"

GenerateHolidays::GenerateHolidays(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::GenerateHolidays)
{
    ui->setupUi(this);

    ui->autoApiKey->setChecked(false);
    ui->autoCountry->setChecked(true);
    ui->autoYear->setChecked(true);

    on_autoApiKey_clicked(false);
    on_autoCountry_clicked(true);
    on_autoYear_clicked(true);
}

GenerateHolidays::~GenerateHolidays()
{
    delete ui;
}

void GenerateHolidays::on_browse_clicked()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Select file to save the holiday list", QDir::homePath() + "/Desktop");
    ui->path->setText(filePath);
}

void GenerateHolidays::on_generate_clicked()
{
    ui->generate->setEnabled(false);

    QString error = "";
    QString filePath, apiKey, countryCode, year;

    filePath = ui->path->text();

    if (ui->autoApiKey->isChecked()) {
        apiKey = "5e6dac9bf1ad8a5681f28cb9ccbabb3354fe405b";
    } else {
        apiKey = ui->customApiKey->text();
    }

    if (ui->autoCountry->isChecked()) {
        countryCode = QLocale::system().name().split("_").at(1);
    } else {
        countryCode = ui->customCountryCode->text();
    }

    if (ui->autoYear->isChecked()) {
        year = QDateTime::currentDateTime().toString("yyyy");
    } else {
        year = ui->customYear->text();
    }

    if (!filePath.count()) {
        error += "Please select a path to save the file.";
    }

    if (!apiKey.count()) {
        error += " Please enter the api key.";
    }

    if (!countryCode.count()) {
        error += " Please enter country code.";
    }

    if (!year.count()) {
        error += " Please enter the year.";
    }

    if (error.count()) {
        QMessageBox m;
        m.setText(error);
        m.show();
        ui->generate->setEnabled(true);
        return;
    }

    qDebug() << filePath;
    qDebug() << year;
    qDebug() << countryCode;
    holidayManage *hDay = new holidayManage(apiKey, countryCode, year);
    connect(hDay, &holidayManage::holidaysCollected, [this, hDay](CalendarData data) {
        saveToFile(data);
        ui->generate->setEnabled(true);
        hDay->deleteLater();
    });
}

void GenerateHolidays::on_autoApiKey_clicked(bool checked)
{
    ui->warning->setVisible(checked);
    ui->customApiKey->setEnabled(!checked);
}

void GenerateHolidays::on_autoCountry_clicked(bool checked)
{
    ui->customCountryCode->setEnabled(!checked);
}

void GenerateHolidays::on_autoYear_clicked(bool checked)
{
    ui->customYear->setEnabled(!checked);
}

void GenerateHolidays::saveToFile(CalendarData data)
{
    QString filePath = ui->path->text();

    // Save the data to holiday event file
    QFile file(filePath);
    if (!file.open(QIODevice::WriteOnly)) {
        //Error code
        qDebug() << "Having problem writing at the holiday event file.";
    }

    QTextStream in(&file);

    Q_FOREACH (QDate d, data.keys()) {
        Q_FOREACH (CalendarEvent e, data[d]) {
            in << QString(d.toString("dd/MM/yyyy") + "," + e.description) + "\n";
        }
    }

    file.close();
}
