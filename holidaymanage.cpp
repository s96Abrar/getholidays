/*
    *
    * This file is a part of GetHolidays.
    * Gets the holiday date and name for individual country and year.
    * Copyright 2019 Abrar
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QUrlQuery>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonDocument>
#include <QNetworkConfigurationManager>

#include "holidaymanage.h"

holidayManage::holidayManage(QString apiKey, QString countryCode, QString year, QObject *parent) : QObject(parent)
{
    // Collected data from calendarific.com
    // APIKEY 5e6dac9bf1ad8a5681f28cb9ccbabb3354fe405b
//    api_key = "5e6dac9bf1ad8a5681f28cb9ccbabb3354fe405b";
//    countryCode = "UK"/*QLocale::system().name().split("_").at(1)*/;
//    year = QString::number(QDate::currentDate().year());

    m_apiKey = apiKey;
    m_countryCode = countryCode;
    m_year = year;

    qDebug() << "Your Country code " << countryCode;
    qDebug() << "Year now " << year;

    nam = new QNetworkAccessManager(this);
    QNetworkConfigurationManager ncm;
    ns = new QNetworkSession(ncm.defaultConfiguration(), this);

    connect(ns, &QNetworkSession::opened, this, &holidayManage::openNetworkSession);

    if (ns->isOpen()) {
        openNetworkSession();
    }

    ns->open();
}

void holidayManage::openNetworkSession()
{
    // Demo https://calendarific.com/api/v2/holidays?api_key=5e6dac9bf1ad8a5681f28cb9ccbabb3354fe405b&country=IN&year=2020
    QUrl url("https://calendarific.com/api/v2/holidays");
    QUrlQuery query;

    query.addQueryItem("api_key", m_apiKey);
    query.addQueryItem("country", m_countryCode);
    query.addQueryItem("year", m_year);

    url.setQuery(query);

    QNetworkReply *rep = nam->get(QNetworkRequest(url));

    connect(rep, &QNetworkReply::finished, [this, rep]() {
        handleHolidayData(rep);
    });
}

void holidayManage::handleHolidayData(QNetworkReply *reply)
{
    if (!reply) {
        qDebug() << "Reply not valid";
        return;
    }

    CalendarData data;

    if (!reply->error()) {
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        qDebug() << reply->readAll();

        if (document.isObject()) {
            QJsonObject obj = document.object();
            QJsonObject tempObject;
            QJsonValue val;

            if (obj.contains("response")) {
                val = obj.value("response");
                tempObject = val.toObject();

                val = tempObject.value("holidays");
                QJsonArray holidayArr = val.toArray();
                int len = holidayArr.size();

                for (int i = 0; i < len; i++) {
                    tempObject = holidayArr.at(i).toObject();
                    QString name = tempObject.value("name").toString();

                    val = tempObject.value("date");
                    QJsonObject dt = val.toObject().value("datetime").toObject();
                    int y = dt.value("year").toInt();
                    int m = dt.value("month").toInt();
                    int d = dt.value("day").toInt();

                    QDate date = QDate(y, m, d);
                    CalendarEvent e;
                    e.type = Type::Holiday;
                    e.description = name;
                    if (data[date].count()) {
                        // Extra overhead because of api returns duplicate data and holiday event
                        bool dup = false;
                        Q_FOREACH (CalendarEvent ce, data[date]) {
                            if (ce.description == name) {
                                dup = true;
                                break;
                            }
                        }

                        if (!dup) {
                            data[date].append(e);
                        }
                    } else {
                        QList <CalendarEvent> list;
                        list.append(e);
                        data[date] = list;
                    }
                }
            }
        }

        emit holidaysCollected(data);
    }
}
