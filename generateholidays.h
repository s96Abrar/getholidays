/*
    *
    * This file is a part of GetHolidays.
    * Gets the holiday date and name for individual country and year.
    * Copyright 2019 Abrar
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#ifndef GENERATEHOLIDAYS_H
#define GENERATEHOLIDAYS_H

#include <QWidget>
#include "global.h"

QT_BEGIN_NAMESPACE
namespace Ui { class GenerateHolidays; }
QT_END_NAMESPACE

class GenerateHolidays : public QWidget
{
    Q_OBJECT

public:
    GenerateHolidays(QWidget *parent = nullptr);
    ~GenerateHolidays();

private slots:
    void on_browse_clicked();
    void on_generate_clicked();
    void on_autoApiKey_clicked(bool checked);
    void on_autoCountry_clicked(bool checked);
    void on_autoYear_clicked(bool checked);

private:
    Ui::GenerateHolidays *ui;
    void saveToFile(CalendarData data);

};
#endif // GENERATEHOLIDAYS_H
